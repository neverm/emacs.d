;; my very own config now
;; change where emacs custom system (UI settings) is going
;; to store it's generated code
(setq custom-file "~/.emacs.d/custom.el")
;; create file if not exists and load it
(write-region "" nil custom-file)
(load custom-file)

(custom-set-variables
 '(require-final-newline t)
 '(line-move-visual t)
 '(next-error-highlight t)
 '(next-error-highlight-no-select t)
 '(next-line-add-newlines nil)
 '(ac-use-fuzzy nil)
 '(backup-directory-alist (quote (("." . "~/.local/share/emacs/backups"))))
 '(compilation-context-lines 2)
 '(compilation-error-screen-columns nil)
 '(compilation-scroll-output t)
 '(compilation-search-path (quote (nil "src")))
 '(electric-indent-mode nil)
 ;; disable tab indentation
 '(indent-tabs-mode nil))

;; General settings

(add-hook 'text-mode-hook 'turn-on-visual-line-mode)
(add-hook 'org-mode-hook 'turn-on-visual-line-mode)

;; disable bell
(setq ring-bell-function 'ignore)

;; org-mode stuff
(require 'org-tempo)
(setq org-src-fontify-natively t)


;; Fonts
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#272822" :foreground "#F8F8F2" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 117 :width normal :foundry "ADBO" :family "DejaVu Sans Mono")))))

;; disable toolbar, scrollbar and menu bar
(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)

;; disable startup screen
(setq inhibit-startup-screen t)

;; highlight matching paretheses
(show-paren-mode 1)

;; better names for multiple files with the same name
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; reopening a buffer will move cursor to the last place
(require 'saveplace)
(setq-default save-place t)

;; Key bindings

(global-set-key [f11] 'speedbar)
(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-]") 'scroll-other-window)
(global-set-key (kbd "M-[") 'scroll-other-window-down)

;; I do not use this character in any of my languages, so remapping
;; unfortunately us(intl) layout doesn't have lambda character
(define-key key-translation-map (kbd "á") (kbd "λ"))

;; Packages

;; ido mode - better buffer selection
(require 'ido)
(ido-mode t)
(setq ido-enable-flex-matching t)

;; package manager
(require 'package)
;; get packages from this source (melpa stable)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; use-package will manage all other packages configuration
;; and installation
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; this will autoinstall everything that will be operated with use-package
(setq use-package-always-ensure t)

;; autoupdate all packages installed with use-package
(use-package auto-package-update
  :config
  (setq auto-package-update-delete-old-versions t)
  (setq auto-package-update-hide-results t)
  (auto-package-update-maybe))

;; handy for editing lisps
(use-package paredit
  :hook (emacs-lisp-mode . paredit-mode)
  :hook (scheme-mode . paredit-mode))

(use-package rainbow-delimiters
  ;; no idea why, but wouldn't work wihtout it even though
  ;; always ensure is set to true
  :ensure t
  :hook (emacs-lisp-mode . rainbow-delimiters-mode)
  :hook (scheme-mode . rainbow-delimiters-mode))

;; (use-package clojure-mode)

;; allows trying out packages without installing them
(use-package try)

(use-package monokai-theme)

(load-theme 'monokai t)

;; for expanding things like p.title into html <p class="title"></p>
(use-package emmet-mode
  :hook (sqml-mode . emmet-mode)
  :hook (css-mode . emmet-mode))

;; disable backups, temp files, etc
(setq auto-save-default nil) ; stop creating #autosave# files
;; This will completely stop emacs from creating temoporary symbolic link file named “#something”
(setq create-lockfiles nil)
;; should consider another option, but so fucking tired by file.ext~ files
(setq make-backup-files nil)

;; Recent files
(use-package recentf-ext)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;; Iedit allows editing multiple instances of the same text simultaneously
;; usually is available in IDEs as rename variable/function, but less smart
(use-package iedit)


;; asm mode stuff
;; change default comment char to #, because I use x86 syntax
(defun set-x86-comment-char () (setq asm-comment-char ?#))
(add-hook 'asm-mode-set-comment-hook 'set-x86-comment-char)
;; set tab width to 4
(defun asm-tab-width-hook () (setq tab-width 4))
(add-hook 'asm-mode-hook 'asm-tab-width-hook)


;; ocaml mode
;;(load-file "~/.emacs.d/my-ocaml.el")

;; LSP and golang stuff
(use-package lsp-mode)
(add-hook 'go-mode-hook #'lsp-deferred)

;; Set up before-save hooks to format buffer and add/delete imports.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

